<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaction = Transaction::latest()->get();

        return response()->json([
            'success' => 'true',
            'message' => 'List All Data transaction',
            'data' => $transaction
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'user_id'   => 'required',
            'book_id'   => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $transaction = Transaction::create([
            'user_id' => $request->user_id,
            'book_id' => $request->book_id,
        ]);

        //success save to database
        if ($transaction) {

            return response()->json([
                'success' => true,
                'message' => 'transaction Created',
                'data'    => $transaction
            ], 201);
        }
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'transaction Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find transaction by ID
        $transaction = Transaction::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data transaction',
            'data'    => $transaction
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'user_id'   => 'required',
            'book_id'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find transaction by ID
        $transaction = Transaction::find($id);

        if ($transaction) {
            //update transaction
            $transaction->update([
                'user_id' => $request->user_id,
                'book_id' => $request->book_id,
                'is_paid' => $request->is_paid,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'transaction Updated',
                'data'    => $transaction
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find transaction by ID
        $transaction = Transaction::findOrfail($id);

        if ($transaction) {
            //delete transaction
            $transaction->delete();

            return response()->json([
                'success' => true,
                'message' => 'transaction Deleted',
            ], 200);
        }

        //data transaction not found
        return response()->json([
            'success' => false,
            'message' => 'transaction Not Found',
        ], 404);
    }
}
