<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::latest()->get();

        return response()->json([
            'success' => 'true',
            'message' => 'List All Data book',
            'data' => $book
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'description'   => 'required',
            'author_id'   => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $book = Book::create([
            'name' => $request->name,
            'description' => $request->description,
            'author_id' => $request->author_id,
        ]);

        //success save to database
        if ($book) {

            return response()->json([
                'success' => true,
                'message' => 'book Created',
                'data'    => $book
            ], 201);
        }
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'book Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find book by ID
        $book = book::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data book',
            'data'    => $book
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'description'   => 'required',
            'author_id'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find book by ID
        $book = Book::find($id);

        if ($book) {
            //update book
            $book->update([
                'name'     => $request->name,
                'description'     => $request->description,
                'author_id'     => $request->author_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'book Updated',
                'data'    => $book
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find book by ID
        $book = Book::find($id);

        if ($book) {
            //delete book
            $book->delete();

            return response()->json([
                'success' => true,
                'message' => 'book Deleted',
            ], 200);
        }

        //data book not found
        return response()->json([
            'success' => false,
            'message' => 'book Not Found',
        ], 404);
    }
}
