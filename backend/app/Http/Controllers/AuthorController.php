<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $author = Author::latest()->get();

        return response()->json([
            'success' => 'true',
            'message' => 'List All Data Author',
            'data' => $author
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $author = Author::create([
            'name' => $request->name,
        ]);

        //success save to database
        if ($author) {

            return response()->json([
                'success' => true,
                'message' => 'Author Created',
                'data'    => $author
            ], 201);
        }
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Aauthor Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.s
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //find author by ID
         $author = Author::findOrfail($id);

         //make response JSON
         return response()->json([
             'success' => true,
             'message' => 'List Data Author',
             'data'    => $author
         ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find author by ID
        $author = Author::find($id);

        if ($author) {
            //update author
            $author->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Author Updated',
                'data'    => $author
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find author by ID
        $author = Author::findOrfail($id);

        if ($author) {
            //delete author
            $author->delete();

            return response()->json([
                'success' => true,
                'message' => 'Author Deleted',
            ], 200);
        }

        //data author not found
        return response()->json([
            'success' => false,
            'message' => 'Author Not Found',
        ], 404);
    }
}
